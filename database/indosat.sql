-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2016 at 10:03 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `indosat`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggotakeluarga`
--

CREATE TABLE `anggotakeluarga` (
  `Kode_Peserta` int(11) NOT NULL,
  `Kode_Citybike` int(11) NOT NULL,
  `Nama` varchar(200) NOT NULL,
  `Jenis_Kelamin` varchar(50) NOT NULL,
  `Ukuran_Jersey` varchar(200) NOT NULL,
  `Usia` varchar(200) NOT NULL,
  `Jenis_Sepeda` varchar(200) NOT NULL,
  `field1` varchar(200) DEFAULT NULL,
  `filed2` varchar(200) DEFAULT NULL,
  `field3` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggotakeluarga`
--

INSERT INTO `anggotakeluarga` (`Kode_Peserta`, `Kode_Citybike`, `Nama`, `Jenis_Kelamin`, `Ukuran_Jersey`, `Usia`, `Jenis_Sepeda`, `field1`, `filed2`, `field3`) VALUES
(4, 5, 'Joice Sibarani', 'Wanita', 'L', '23', 'MTB', NULL, NULL, NULL),
(5, 5, 'Endy Octora', 'Wanita', 'L', '23', 'Road Bike', NULL, NULL, NULL),
(6, 6, 'Joshua', 'Pria', 'S', '18', 'Road Bike', NULL, NULL, NULL),
(7, 6, 'Madani', 'Pria', 'S', '19', 'Road Bike', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('Super Administrator', '2', 1474814502);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, 1474909644, 1474909644),
('/administrator/*', 2, NULL, NULL, NULL, 1463900830, 1463900830),
('/administrator/role/*', 2, NULL, NULL, NULL, 1463900831, 1463900831),
('/administrator/role/create', 2, NULL, NULL, NULL, 1463900832, 1463900832),
('/administrator/role/delete', 2, NULL, NULL, NULL, 1463900833, 1463900833),
('/administrator/role/index', 2, NULL, NULL, NULL, 1463900834, 1463900834),
('/administrator/role/permission', 2, NULL, NULL, NULL, 1463900835, 1463900835),
('/administrator/role/update', 2, NULL, NULL, NULL, 1463900836, 1463900836),
('/administrator/role/updateizin', 2, NULL, NULL, NULL, 1463900837, 1463900837),
('/administrator/role/view', 2, NULL, NULL, NULL, 1463900839, 1463900839),
('/administrator/route/*', 2, NULL, NULL, NULL, 1463900841, 1463900841),
('/administrator/route/create', 2, NULL, NULL, NULL, 1463900842, 1463900842),
('/administrator/route/delete', 2, NULL, NULL, NULL, 1463900843, 1463900843),
('/administrator/route/generate', 2, NULL, NULL, NULL, 1463900844, 1463900844),
('/administrator/route/index', 2, NULL, NULL, NULL, 1463900845, 1463900845),
('/administrator/route/update', 2, NULL, NULL, NULL, 1463900845, 1463900845),
('/administrator/route/view', 2, NULL, NULL, NULL, 1463900847, 1463900847),
('/administrator/user/*', 2, NULL, NULL, NULL, 1463900848, 1463900848),
('/administrator/user/create', 2, NULL, NULL, NULL, 1463900850, 1463900850),
('/administrator/user/delete', 2, NULL, NULL, NULL, 1463900853, 1463900853),
('/administrator/user/index', 2, NULL, NULL, NULL, 1463900852, 1463900852),
('/administrator/user/update', 2, NULL, NULL, NULL, 1463900854, 1463900854),
('/administrator/user/view', 2, NULL, NULL, NULL, 1463900856, 1463900856),
('/akun/*', 2, NULL, NULL, NULL, 1463993414, 1463993414),
('/akun/create', 2, NULL, NULL, NULL, 1463993417, 1463993417),
('/akun/delete', 2, NULL, NULL, NULL, 1463993418, 1463993418),
('/akun/export-excel', 2, NULL, NULL, NULL, 1470581628, 1470581628),
('/akun/index', 2, NULL, NULL, NULL, 1463941007, 1463941007),
('/akun/update', 2, NULL, NULL, NULL, 1463993420, 1463993420),
('/akun/view', 2, NULL, NULL, NULL, 1463941579, 1463941579),
('/belanja/*', 2, NULL, NULL, NULL, 1464018430, 1464018430),
('/belanja/create', 2, NULL, NULL, NULL, 1464018430, 1464018430),
('/belanja/delete', 2, NULL, NULL, NULL, 1463938348, 1463938348),
('/belanja/export-excel', 2, NULL, NULL, NULL, 1470581628, 1470581628),
('/belanja/index', 2, NULL, NULL, NULL, 1463938349, 1463938349),
('/belanja/update', 2, NULL, NULL, NULL, 1463938350, 1463938350),
('/belanja/view', 2, NULL, NULL, NULL, 1463938350, 1463938350),
('/debug/*', 2, NULL, NULL, NULL, 1470581623, 1470581623),
('/debug/default/*', 2, NULL, NULL, NULL, 1470581624, 1470581624),
('/debug/default/db-explain', 2, NULL, NULL, NULL, 1470581624, 1470581624),
('/debug/default/download-mail', 2, NULL, NULL, NULL, 1470581625, 1470581625),
('/debug/default/index', 2, NULL, NULL, NULL, 1470581625, 1470581625),
('/debug/default/toolbar', 2, NULL, NULL, NULL, 1470581626, 1470581626),
('/debug/default/view', 2, NULL, NULL, NULL, 1470581627, 1470581627),
('/desa/*', 2, NULL, NULL, NULL, 1463900866, 1463900866),
('/desa/create', 2, NULL, NULL, NULL, 1463900867, 1463900867),
('/desa/delete', 2, NULL, NULL, NULL, 1463900869, 1463900869),
('/desa/index', 2, NULL, NULL, NULL, 1463900871, 1463900871),
('/desa/lists', 2, NULL, NULL, NULL, 1463900872, 1463900872),
('/desa/update', 2, NULL, NULL, NULL, 1463900873, 1463900873),
('/desa/view', 2, NULL, NULL, NULL, 1463900874, 1463900874),
('/gii/*', 2, NULL, NULL, NULL, 1470581631, 1470581631),
('/gii/default/*', 2, NULL, NULL, NULL, 1470581632, 1470581632),
('/gii/default/action', 2, NULL, NULL, NULL, 1470581634, 1470581634),
('/gii/default/diff', 2, NULL, NULL, NULL, 1470581634, 1470581634),
('/gii/default/index', 2, NULL, NULL, NULL, 1470581637, 1470581637),
('/gii/default/preview', 2, NULL, NULL, NULL, 1470581639, 1470581639),
('/gii/default/view', 2, NULL, NULL, NULL, 1470581639, 1470581639),
('/indikator/*', 2, NULL, NULL, NULL, 1470581632, 1470581632),
('/indikator/create', 2, NULL, NULL, NULL, 1463900886, 1463900886),
('/indikator/delete', 2, NULL, NULL, NULL, 1463900887, 1463900887),
('/indikator/export-excel', 2, NULL, NULL, NULL, 1470581640, 1470581640),
('/indikator/index', 2, NULL, NULL, NULL, 1463900888, 1463900888),
('/indikator/update', 2, NULL, NULL, NULL, 1463900969, 1463900969),
('/indikator/view', 2, NULL, NULL, NULL, 1463900970, 1463900970),
('/jenisbelanja/*', 2, NULL, NULL, NULL, 1470581633, 1470581633),
('/jenisbelanja/create', 2, NULL, NULL, NULL, 1463987654, 1463987654),
('/jenisbelanja/delete', 2, NULL, NULL, NULL, 1463987654, 1463987654),
('/jenisbelanja/export-excel', 2, NULL, NULL, NULL, 1470581640, 1470581640),
('/jenisbelanja/getkoderefjenisbelanja', 2, NULL, NULL, NULL, 1473156644, 1473156644),
('/jenisbelanja/index', 2, NULL, NULL, NULL, 1463987656, 1463987656),
('/jenisbelanja/update', 2, NULL, NULL, NULL, 1463987658, 1463987658),
('/jenisbelanja/view', 2, NULL, NULL, NULL, 1463987658, 1463987658),
('/kategorisatuanharga/*', 2, NULL, NULL, NULL, 1464018961, 1464018961),
('/kategorisatuanharga/create', 2, NULL, NULL, NULL, 1470581642, 1470581642),
('/kategorisatuanharga/delete', 2, NULL, NULL, NULL, 1470581641, 1470581641),
('/kategorisatuanharga/export-excel', 2, NULL, NULL, NULL, 1470581643, 1470581643),
('/kategorisatuanharga/getkoderefkategorisatuanharga', 2, NULL, NULL, NULL, 1473215354, 1473215354),
('/kategorisatuanharga/index', 2, NULL, NULL, NULL, 1470581644, 1470581644),
('/kategorisatuanharga/update', 2, NULL, NULL, NULL, 1470581645, 1470581645),
('/kategorisatuanharga/view', 2, NULL, NULL, NULL, 1470581645, 1470581645),
('/kecamatan/*', 2, NULL, NULL, NULL, 1470581653, 1470581653),
('/kecamatan/create', 2, NULL, NULL, NULL, 1463900965, 1463900965),
('/kecamatan/delete', 2, NULL, NULL, NULL, 1463900966, 1463900966),
('/kecamatan/export-excel', 2, NULL, NULL, NULL, 1470581649, 1470581649),
('/kecamatan/index', 2, NULL, NULL, NULL, 1463900967, 1463900967),
('/kecamatan/update', 2, NULL, NULL, NULL, 1463900968, 1463900968),
('/kecamatan/view', 2, NULL, NULL, NULL, 1463900969, 1463900969),
('/kuappas/*', 2, NULL, NULL, NULL, 1470581652, 1470581652),
('/kuappas/create', 2, NULL, NULL, NULL, 1470581651, 1470581651),
('/kuappas/delete', 2, NULL, NULL, NULL, 1470581650, 1470581650),
('/kuappas/export-excel', 2, NULL, NULL, NULL, 1470581648, 1470581648),
('/kuappas/index', 2, NULL, NULL, NULL, 1470581648, 1470581648),
('/kuappas/update', 2, NULL, NULL, NULL, 1470581646, 1470581646),
('/kuappas/view', 2, NULL, NULL, NULL, 1470581646, 1470581646),
('/limitsumberdana/*', 2, NULL, NULL, NULL, 1471583163, 1471583163),
('/limitsumberdana/create', 2, NULL, NULL, NULL, 1471583164, 1471583164),
('/limitsumberdana/delete', 2, NULL, NULL, NULL, 1471583164, 1471583164),
('/limitsumberdana/index', 2, NULL, NULL, NULL, 1471583166, 1471583166),
('/limitsumberdana/update', 2, NULL, NULL, NULL, 1471583166, 1471583166),
('/limitsumberdana/view', 2, NULL, NULL, NULL, 1471583167, 1471583167),
('/locking/*', 2, NULL, NULL, NULL, 1470581652, 1470581652),
('/locking/create', 2, NULL, NULL, NULL, 1470581651, 1470581651),
('/locking/delete', 2, NULL, NULL, NULL, 1470581650, 1470581650),
('/locking/index', 2, NULL, NULL, NULL, 1470581649, 1470581649),
('/locking/update', 2, NULL, NULL, NULL, 1470581648, 1470581648),
('/locking/view', 2, NULL, NULL, NULL, 1470581647, 1470581647),
('/lockingsystem/*', 2, NULL, NULL, NULL, 1464018360, 1464018360),
('/lockingsystem/create', 2, NULL, NULL, NULL, 1464018361, 1464018361),
('/lockingsystem/delete', 2, NULL, NULL, NULL, 1464018362, 1464018362),
('/lockingsystem/index', 2, NULL, NULL, NULL, 1464018363, 1464018363),
('/lockingsystem/update', 2, NULL, NULL, NULL, 1464018365, 1464018365),
('/lockingsystem/view', 2, NULL, NULL, NULL, 1464018365, 1464018365),
('/log/*', 2, NULL, NULL, NULL, 1464018360, 1464018360),
('/log/index', 2, NULL, NULL, NULL, 1464018361, 1464018361),
('/log/view', 2, NULL, NULL, NULL, 1464018363, 1464018363),
('/mimin/*', 2, NULL, NULL, NULL, 1463900973, 1463900973),
('/mimin/role/*', 2, NULL, NULL, NULL, 1463900973, 1463900973),
('/mimin/role/create', 2, NULL, NULL, NULL, 1463900975, 1463900975),
('/mimin/role/delete', 2, NULL, NULL, NULL, 1463900976, 1463900976),
('/mimin/role/index', 2, NULL, NULL, NULL, 1463900977, 1463900977),
('/mimin/role/permission', 2, NULL, NULL, NULL, 1463900978, 1463900978),
('/mimin/role/update', 2, NULL, NULL, NULL, 1463900979, 1463900979),
('/mimin/role/view', 2, NULL, NULL, NULL, 1463900980, 1463900980),
('/mimin/route/*', 2, NULL, NULL, NULL, 1463900981, 1463900981),
('/mimin/route/create', 2, NULL, NULL, NULL, 1463900982, 1463900982),
('/mimin/route/delete', 2, NULL, NULL, NULL, 1463900986, 1463900986),
('/mimin/route/generate', 2, NULL, NULL, NULL, 1463901014, 1463901014),
('/mimin/route/index', 2, NULL, NULL, NULL, 1463901015, 1463901015),
('/mimin/route/update', 2, NULL, NULL, NULL, 1463901017, 1463901017),
('/mimin/route/view', 2, NULL, NULL, NULL, 1463901018, 1463901018),
('/mimin/user/*', 2, NULL, NULL, NULL, 1463901010, 1463901010),
('/mimin/user/create', 2, NULL, NULL, NULL, 1463901010, 1463901010),
('/mimin/user/delete', 2, NULL, NULL, NULL, 1463901012, 1463901012),
('/mimin/user/index', 2, NULL, NULL, NULL, 1463901013, 1463901013),
('/mimin/user/update', 2, NULL, NULL, NULL, 1463901015, 1463901015),
('/mimin/user/view', 2, NULL, NULL, NULL, 1463901016, 1463901016),
('/musrenbang/*', 2, NULL, NULL, NULL, 1470581868, 1470581868),
('/musrenbang/awal', 2, NULL, NULL, NULL, 1470581655, 1470581655),
('/musrenbang/create', 2, NULL, NULL, NULL, 1470581657, 1470581657),
('/musrenbang/createtersedia', 2, NULL, NULL, NULL, 1473265039, 1473265039),
('/musrenbang/data', 2, NULL, NULL, NULL, 1470581658, 1470581658),
('/musrenbang/delete', 2, NULL, NULL, NULL, 1470581659, 1470581659),
('/musrenbang/export-excel', 2, NULL, NULL, NULL, 1470581659, 1470581659),
('/musrenbang/index', 2, NULL, NULL, NULL, 1470581661, 1470581661),
('/musrenbang/lists', 2, NULL, NULL, NULL, 1464018373, 1464018373),
('/musrenbang/update', 2, NULL, NULL, NULL, 1470581661, 1470581661),
('/musrenbang/view', 2, NULL, NULL, NULL, 1470581663, 1470581663),
('/musrenbangkecamatan/*', 2, NULL, NULL, NULL, 1470320985, 1470320985),
('/musrenbangkecamatan/awal', 2, NULL, NULL, NULL, 1464018369, 1464018369),
('/musrenbangkecamatan/create', 2, NULL, NULL, NULL, 1463901028, 1463901028),
('/musrenbangkecamatan/createtersedia', 2, NULL, NULL, NULL, 1473265039, 1473265039),
('/musrenbangkecamatan/data', 2, NULL, NULL, NULL, 1464018370, 1464018370),
('/musrenbangkecamatan/dataawal', 2, NULL, NULL, NULL, 1464018371, 1464018371),
('/musrenbangkecamatan/delete', 2, NULL, NULL, NULL, 1463901029, 1463901029),
('/musrenbangkecamatan/export-excel', 2, NULL, NULL, NULL, 1464018373, 1464018373),
('/musrenbangkecamatan/index', 2, NULL, NULL, NULL, 1463901031, 1463901031),
('/musrenbangkecamatan/lists', 2, NULL, NULL, NULL, 1464018374, 1464018374),
('/musrenbangkecamatan/listskecamatan', 2, NULL, NULL, NULL, 1473349484, 1473349484),
('/musrenbangkecamatan/update', 2, NULL, NULL, NULL, 1463901033, 1463901033),
('/musrenbangkecamatan/view', 2, NULL, NULL, NULL, 1463901036, 1463901036),
('/organisasi/*', 2, NULL, NULL, NULL, 1464876212, 1464876212),
('/organisasi/create', 2, NULL, NULL, NULL, 1463901038, 1463901038),
('/organisasi/delete', 2, NULL, NULL, NULL, 1463901042, 1463901042),
('/organisasi/export-excel', 2, NULL, NULL, NULL, 1470581670, 1470581670),
('/organisasi/index', 2, NULL, NULL, NULL, 1463901041, 1463901041),
('/organisasi/lists', 2, NULL, NULL, NULL, 1470581670, 1470581670),
('/organisasi/update', 2, NULL, NULL, NULL, 1463901044, 1463901044),
('/organisasi/view', 2, NULL, NULL, NULL, 1463901045, 1463901045),
('/pimpinankecamatan/*', 2, NULL, NULL, NULL, 1470581665, 1470581665),
('/pimpinankecamatan/create', 2, NULL, NULL, NULL, 1463901090, 1463901090),
('/pimpinankecamatan/delete', 2, NULL, NULL, NULL, 1463901085, 1463901085),
('/pimpinankecamatan/export-excel', 2, NULL, NULL, NULL, 1470581669, 1470581669),
('/pimpinankecamatan/index', 2, NULL, NULL, NULL, 1463901081, 1463901081),
('/pimpinankecamatan/update', 2, NULL, NULL, NULL, 1463901074, 1463901074),
('/pimpinankecamatan/view', 2, NULL, NULL, NULL, 1463901074, 1463901074),
('/pimpinanskpd/*', 2, NULL, NULL, NULL, 1470581666, 1470581666),
('/pimpinanskpd/create', 2, NULL, NULL, NULL, 1463901089, 1463901089),
('/pimpinanskpd/delete', 2, NULL, NULL, NULL, 1463901084, 1463901084),
('/pimpinanskpd/export-excel', 2, NULL, NULL, NULL, 1470581669, 1470581669),
('/pimpinanskpd/index', 2, NULL, NULL, NULL, 1463901081, 1463901081),
('/pimpinanskpd/update', 2, NULL, NULL, NULL, 1463901075, 1463901075),
('/pimpinanskpd/view', 2, NULL, NULL, NULL, 1463901073, 1463901073),
('/program/*', 2, NULL, NULL, NULL, 1470581667, 1470581667),
('/program/create', 2, NULL, NULL, NULL, 1463901088, 1463901088),
('/program/delete', 2, NULL, NULL, NULL, 1463901084, 1463901084),
('/program/export-excel', 2, NULL, NULL, NULL, 1470581668, 1470581668),
('/program/index', 2, NULL, NULL, NULL, 1463901080, 1463901080),
('/program/lists', 2, NULL, NULL, NULL, 1464269556, 1464269556),
('/program/programs-by-rkpd', 2, NULL, NULL, NULL, 1470581672, 1470581672),
('/program/update', 2, NULL, NULL, NULL, 1463901072, 1463901072),
('/program/updatefromuser', 2, NULL, NULL, NULL, 1470581674, 1470581674),
('/program/view', 2, NULL, NULL, NULL, 1463901071, 1463901071),
('/rka/*', 2, NULL, NULL, NULL, 1464018383, 1464018383),
('/rka/create', 2, NULL, NULL, NULL, 1464018385, 1464018385),
('/rka/delete', 2, NULL, NULL, NULL, 1464018401, 1464018401),
('/rka/export-excel', 2, NULL, NULL, NULL, 1464018402, 1464018402),
('/rka/index', 2, NULL, NULL, NULL, 1464018404, 1464018404),
('/rka/pilihorganisasi', 2, NULL, NULL, NULL, 1464018406, 1464018406),
('/rka/pilihskpd', 2, NULL, NULL, NULL, 1470581676, 1470581676),
('/rka/update', 2, NULL, NULL, NULL, 1464018407, 1464018407),
('/rka/view', 2, NULL, NULL, NULL, 1464018408, 1464018408),
('/rkaindikator/*', 2, NULL, NULL, NULL, 1470582198, 1470582198),
('/rkaindikator/create', 2, NULL, NULL, NULL, 1464018384, 1464018384),
('/rkaindikator/delete', 2, NULL, NULL, NULL, 1464018401, 1464018401),
('/rkaindikator/index', 2, NULL, NULL, NULL, 1464018402, 1464018402),
('/rkaindikator/update', 2, NULL, NULL, NULL, 1464018404, 1464018404),
('/rkaindikator/view', 2, NULL, NULL, NULL, 1464018406, 1464018406),
('/rkasatuanharga/*', 2, NULL, NULL, NULL, 1464018382, 1464018382),
('/rkasatuanharga/create', 2, NULL, NULL, NULL, 1464018386, 1464018386),
('/rkasatuanharga/delete', 2, NULL, NULL, NULL, 1464018400, 1464018400),
('/rkasatuanharga/deleteall', 2, NULL, NULL, NULL, 1470581677, 1470581677),
('/rkasatuanharga/index', 2, NULL, NULL, NULL, 1464018403, 1464018403),
('/rkasatuanharga/update', 2, NULL, NULL, NULL, 1464018404, 1464018404),
('/rkasatuanharga/view', 2, NULL, NULL, NULL, 1464018405, 1464018405),
('/rkasatuanhargahistorikal/*', 2, NULL, NULL, NULL, 1464018381, 1464018381),
('/rkasatuanhargahistorikal/create', 2, NULL, NULL, NULL, 1464018386, 1464018386),
('/rkasatuanhargahistorikal/delete', 2, NULL, NULL, NULL, 1464018400, 1464018400),
('/rkasatuanhargahistorikal/index', 2, NULL, NULL, NULL, 1464018399, 1464018399),
('/rkasatuanhargahistorikal/update', 2, NULL, NULL, NULL, 1464018398, 1464018398),
('/rkasatuanhargahistorikal/view', 2, NULL, NULL, NULL, 1464018398, 1464018398),
('/rkpd/*', 2, NULL, NULL, NULL, 1464018380, 1464018380),
('/rkpd/delete', 2, NULL, NULL, NULL, 1464018387, 1464018387),
('/rkpd/detailusulankecamatan', 2, NULL, NULL, NULL, 1464018389, 1464018389),
('/rkpd/detailusulanskpd', 2, NULL, NULL, NULL, 1464018390, 1464018390),
('/rkpd/export-excel', 2, NULL, NULL, NULL, 1464018392, 1464018392),
('/rkpd/export-excel2', 2, NULL, NULL, NULL, 1464018393, 1464018393),
('/rkpd/export-pdf', 2, NULL, NULL, NULL, 1464018394, 1464018394),
('/rkpd/exportdata', 2, NULL, NULL, NULL, 1470320056, 1470320056),
('/rkpd/hapususulankecamatan', 2, NULL, NULL, NULL, 1464018395, 1464018395),
('/rkpd/hapususulanskpd', 2, NULL, NULL, NULL, 1464018397, 1464018397),
('/rkpd/index', 2, NULL, NULL, NULL, 1464018396, 1464018396),
('/rkpd/tambahusulankecamatan', 2, NULL, NULL, NULL, 1464018379, 1464018379),
('/rkpd/tambahusulanskpd', 2, NULL, NULL, NULL, 1464018387, 1464018387),
('/rkpd/usulankecamatan', 2, NULL, NULL, NULL, 1464018388, 1464018388),
('/rkpd/usulanskpd', 2, NULL, NULL, NULL, 1464018391, 1464018391),
('/rkpd/view', 2, NULL, NULL, NULL, 1464018392, 1464018392),
('/satuanharga/*', 2, NULL, NULL, NULL, 1464018378, 1464018378),
('/satuanharga/create', 2, NULL, NULL, NULL, 1464018417, 1464018417),
('/satuanharga/delete', 2, NULL, NULL, NULL, 1464018416, 1464018416),
('/satuanharga/export-excel', 2, NULL, NULL, NULL, 1470581680, 1470581680),
('/satuanharga/index', 2, NULL, NULL, NULL, 1464018414, 1464018414),
('/satuanharga/update', 2, NULL, NULL, NULL, 1464018412, 1464018412),
('/satuanharga/view', 2, NULL, NULL, NULL, 1464018412, 1464018412),
('/satuanhargahistorikal/*', 2, NULL, NULL, NULL, 1464018378, 1464018378),
('/satuanhargahistorikal/create', 2, NULL, NULL, NULL, 1464018417, 1464018417),
('/satuanhargahistorikal/delete', 2, NULL, NULL, NULL, 1464018415, 1464018415),
('/satuanhargahistorikal/index', 2, NULL, NULL, NULL, 1464018413, 1464018413),
('/satuanhargahistorikal/update', 2, NULL, NULL, NULL, 1464018411, 1464018411),
('/satuanhargahistorikal/view', 2, NULL, NULL, NULL, 1464018410, 1464018410),
('/site/*', 2, NULL, NULL, NULL, 1470319236, 1470319236),
('/site/about', 2, NULL, NULL, NULL, 1463901086, 1463901086),
('/site/captcha', 2, NULL, NULL, NULL, 1463901082, 1463901082),
('/site/changepassword', 2, NULL, NULL, NULL, 1471257805, 1471257805),
('/site/contact', 2, NULL, NULL, NULL, 1463901078, 1463901078),
('/site/error', 2, NULL, NULL, NULL, 1463901067, 1463901067),
('/site/index', 2, NULL, NULL, NULL, 1470582371, 1470582371),
('/site/login', 2, NULL, NULL, NULL, 1463901069, 1463901069),
('/site/logout', 2, NULL, NULL, NULL, 1463901070, 1463901070),
('/skpd/*', 2, NULL, NULL, NULL, 1470319715, 1470319715),
('/skpd/create', 2, NULL, NULL, NULL, 1463901062, 1463901062),
('/skpd/export-excel', 2, NULL, NULL, NULL, 1470581697, 1470581697),
('/skpd/index', 2, NULL, NULL, NULL, 1463901063, 1463901063),
('/skpd/lists', 2, NULL, NULL, NULL, 1473349597, 1473349597),
('/skpd/update', 2, NULL, NULL, NULL, 1463901066, 1463901066),
('/skpd/view', 2, NULL, NULL, NULL, 1463901066, 1463901066),
('/statistic/*', 2, NULL, NULL, NULL, 1470319254, 1470319254),
('/statistic/danaskpd', 2, NULL, NULL, NULL, 1470319254, 1470319254),
('/statistic/index', 2, NULL, NULL, NULL, 1470319255, 1470319255),
('/sumberdana/*', 2, NULL, NULL, NULL, 1470581688, 1470581688),
('/sumberdana/create', 2, NULL, NULL, NULL, 1463901059, 1463901059),
('/sumberdana/delete', 2, NULL, NULL, NULL, 1463901059, 1463901059),
('/sumberdana/export-excel', 2, NULL, NULL, NULL, 1470581699, 1470581699),
('/sumberdana/index', 2, NULL, NULL, NULL, 1463901058, 1463901058),
('/sumberdana/update', 2, NULL, NULL, NULL, 1463901056, 1463901056),
('/sumberdana/view', 2, NULL, NULL, NULL, 1463901056, 1463901056),
('/tipebelanja/*', 2, NULL, NULL, NULL, 1463978781, 1463978781),
('/tipebelanja/create', 2, NULL, NULL, NULL, 1463978782, 1463978782),
('/tipebelanja/delete', 2, NULL, NULL, NULL, 1463978784, 1463978784),
('/tipebelanja/export-excel', 2, NULL, NULL, NULL, 1470581700, 1470581700),
('/tipebelanja/index', 2, NULL, NULL, NULL, 1463978783, 1463978783),
('/tipebelanja/update', 2, NULL, NULL, NULL, 1463978785, 1463978785),
('/tipebelanja/view', 2, NULL, NULL, NULL, 1463978786, 1463978786),
('/urusan/*', 2, NULL, NULL, NULL, 1463901050, 1463901050),
('/urusan/create', 2, NULL, NULL, NULL, 1463901052, 1463901052),
('/urusan/delete', 2, NULL, NULL, NULL, 1463901052, 1463901052),
('/urusan/export-excel', 2, NULL, NULL, NULL, 1464018420, 1464018420),
('/urusan/index', 2, NULL, NULL, NULL, 1463901053, 1463901053),
('/urusan/update', 2, NULL, NULL, NULL, 1463901054, 1463901054),
('/urusan/view', 2, NULL, NULL, NULL, 1463901055, 1463901055),
('/user/*', 2, NULL, NULL, NULL, 1463901048, 1463901048),
('/user/index', 2, NULL, NULL, NULL, 1463901049, 1463901049),
('Super Administrator', 1, NULL, NULL, NULL, 1459356450, 1461942285);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Super Administrator', '/*');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `charitywalk`
--

CREATE TABLE `charitywalk` (
  `Kode_Charitywalk` int(11) NOT NULL,
  `Nama_Team` varchar(250) NOT NULL,
  `Email_Perwakilan` varchar(50) NOT NULL,
  `No_HP` varchar(250) NOT NULL,
  `Keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `charitywalk`
--

INSERT INTO `charitywalk` (`Kode_Charitywalk`, `Nama_Team`, `Email_Perwakilan`, `No_HP`, `Keterangan`) VALUES
(2, 'SPLASH TEAM', 'bernard@splash-idea.com', '082311112222', '-'),
(3, 'RKI TEAM', 'gunawan@kasih-group.com', '082355554444', '-'),
(4, 'Restu Team', 'restu@mail.com', '0988888888', '-'),
(5, 'SK Team', 'sk@mail.com', '0828219373', '-'),
(6, 'KK', 'kk@mail.com', '09557654467', '-'),
(7, 'CK', 'ck@mail.com', '788672436723467', '-'),
(8, 'Del Walk', 'jordan.sipahutar@gmail.com', '082311648515', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `citybike`
--

CREATE TABLE `citybike` (
  `Kode_Citybike` int(11) NOT NULL,
  `Nama_Peserta` varchar(250) NOT NULL,
  `Jenis_Kelamin` varchar(200) NOT NULL,
  `Email_Peserta` varchar(100) NOT NULL,
  `No_HP` varchar(100) NOT NULL,
  `NIK` varchar(100) NOT NULL,
  `Usia` varchar(200) NOT NULL,
  `Kategori_City_Bike` varchar(100) NOT NULL,
  `Ukuran_Jersey` varchar(100) NOT NULL,
  `Nama_Anggota_Keluarga` varchar(100) DEFAULT NULL,
  `Keterangan` text,
  `field1` varchar(200) DEFAULT NULL,
  `field2` varchar(200) DEFAULT NULL,
  `field3` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `citybike`
--

INSERT INTO `citybike` (`Kode_Citybike`, `Nama_Peserta`, `Jenis_Kelamin`, `Email_Peserta`, `No_HP`, `NIK`, `Usia`, `Kategori_City_Bike`, `Ukuran_Jersey`, `Nama_Anggota_Keluarga`, `Keterangan`, `field1`, `field2`, `field3`) VALUES
(5, 'Jordan Hakiki Sipahutar', 'Pria', 'jordan@mail.com', '09567124351734', '11111095', '23', '49 Km', 'M', 'IT', 'MTB', NULL, NULL, NULL),
(6, 'Joel Sipahutar', 'Pria', 'mail@h.com', '8767678', '11111096', '20', '49 Km', 'L', 'IT', 'Road Bike', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `futsal`
--

CREATE TABLE `futsal` (
  `Kode_Futsal` int(11) NOT NULL,
  `Nama_Team` varchar(250) NOT NULL,
  `Nama_Manager_Team` varchar(250) NOT NULL,
  `Email_Perwakilan` varchar(200) NOT NULL,
  `No_HP` varchar(250) NOT NULL,
  `Keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `futsal`
--

INSERT INTO `futsal` (`Kode_Futsal`, `Nama_Team`, `Nama_Manager_Team`, `Email_Perwakilan`, `No_HP`, `Keterangan`) VALUES
(2, 'Gulamo FC', 'Jordan', 'jordan@mail.com', '0823116512521', '-');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1474813986);

-- --------------------------------------------------------

--
-- Table structure for table `pesertacharitywalk`
--

CREATE TABLE `pesertacharitywalk` (
  `Kode_Peserta` int(11) NOT NULL,
  `Kode_Charitywalk` int(11) NOT NULL,
  `Nama` varchar(250) NOT NULL,
  `NIK` varchar(250) NOT NULL,
  `Jenis_Kelamin` varchar(250) NOT NULL,
  `No_HP` varchar(250) DEFAULT NULL,
  `Keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesertacharitywalk`
--

INSERT INTO `pesertacharitywalk` (`Kode_Peserta`, `Kode_Charitywalk`, `Nama`, `NIK`, `Jenis_Kelamin`, `No_HP`, `Keterangan`) VALUES
(3, 2, 'Bernard', '11111095', 'Pria', '08234444333', NULL),
(4, 2, 'Strow', '11111096', 'Pria', '', NULL),
(5, 2, 'Rully', '11111097', 'Pria', '', NULL),
(6, 2, 'Hakim', '11111096', 'Pria', '', NULL),
(7, 3, 'Gunawan', '11111001', 'Pria', '', NULL),
(8, 3, 'Hendra Ali', '11111002', 'Pria', '', NULL),
(9, 4, 'Didit', '111111004', 'Pria', '', NULL),
(10, 4, 'Sugandi', '11111006', 'Pria', '', NULL),
(11, 5, 'Iliana', '111078', 'Wanita', '', NULL),
(12, 5, 'Agus', '111678', 'Pria', '', NULL),
(13, 6, 'Surya', '74753425134', 'Pria', '', NULL),
(14, 6, 'Ariska', '79845386546', 'Pria', '', NULL),
(21, 7, 'Sugeng', '78634267342', 'Pria', '', NULL),
(22, 8, 'Jordan', '18447748312', 'Pria', '082666666', 'jordan@mail.com'),
(23, 8, 'Hakiki', '67843276', 'Pria', '09555', 'mail2@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `pesertafutsal`
--

CREATE TABLE `pesertafutsal` (
  `Kode_Peserta` int(11) NOT NULL,
  `Kode_Futsal` int(11) NOT NULL,
  `Nama` varchar(250) NOT NULL,
  `NIK` varchar(250) NOT NULL,
  `Jenis_Kelamin` varchar(50) NOT NULL,
  `No_HP` varchar(100) DEFAULT NULL,
  `Keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesertafutsal`
--

INSERT INTO `pesertafutsal` (`Kode_Peserta`, `Kode_Futsal`, `Nama`, `NIK`, `Jenis_Kelamin`, `No_HP`, `Keterangan`) VALUES
(7, 2, 'Jordan', '6784267', 'Pria', '', NULL),
(8, 2, 'Arie', '87987532', 'Pria', '', NULL),
(9, 2, 'Samuel', '8793474832', 'Pria', '', NULL),
(10, 2, 'Erikson', '678434327', 'Pria', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE `route` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`name`, `alias`, `type`, `status`) VALUES
('/*', '*', '', 1),
('/administrator/*', '*', 'administrator', 1),
('/administrator/role/*', '*', 'administrator/role', 1),
('/administrator/role/create', 'create', 'administrator/role', 1),
('/administrator/role/delete', 'delete', 'administrator/role', 1),
('/administrator/role/index', 'index', 'administrator/role', 1),
('/administrator/role/permission', 'permission', 'administrator/role', 1),
('/administrator/role/update', 'update', 'administrator/role', 1),
('/administrator/role/view', 'view', 'administrator/role', 1),
('/administrator/route/*', '*', 'administrator/route', 1),
('/administrator/route/create', 'create', 'administrator/route', 1),
('/administrator/route/delete', 'delete', 'administrator/route', 1),
('/administrator/route/generate', 'generate', 'administrator/route', 1),
('/administrator/route/index', 'index', 'administrator/route', 1),
('/administrator/route/update', 'update', 'administrator/route', 1),
('/administrator/route/view', 'view', 'administrator/route', 1),
('/administrator/user/*', '*', 'administrator/user', 1),
('/administrator/user/create', 'create', 'administrator/user', 1),
('/administrator/user/delete', 'delete', 'administrator/user', 1),
('/administrator/user/index', 'index', 'administrator/user', 1),
('/administrator/user/update', 'update', 'administrator/user', 1),
('/administrator/user/view', 'view', 'administrator/user', 1),
('/debug/*', '*', 'debug', 1),
('/debug/default/*', '*', 'debug/default', 1),
('/debug/default/db-explain', 'db-explain', 'debug/default', 1),
('/debug/default/download-mail', 'download-mail', 'debug/default', 1),
('/debug/default/index', 'index', 'debug/default', 1),
('/debug/default/toolbar', 'toolbar', 'debug/default', 1),
('/debug/default/view', 'view', 'debug/default', 1),
('/gii/*', '*', 'gii', 1),
('/gii/default/*', '*', 'gii/default', 1),
('/gii/default/action', 'action', 'gii/default', 1),
('/gii/default/diff', 'diff', 'gii/default', 1),
('/gii/default/index', 'index', 'gii/default', 1),
('/gii/default/preview', 'preview', 'gii/default', 1),
('/gii/default/view', 'view', 'gii/default', 1),
('/site/*', '*', 'site', 1),
('/site/about', 'about', 'site', 1),
('/site/captcha', 'captcha', 'site', 1),
('/site/changepassword', 'changepassword', 'site', 1),
('/site/contact', 'contact', 'site', 1),
('/site/error', 'error', 'site', 1),
('/site/index', 'index', 'site', 1),
('/site/login', 'login', 'site', 1),
('/site/logout', 'logout', 'site', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(2, 'admin', 'MUHN4QX3fLRw6zaOB3lv9vqyucR-hCOB', '$2y$13$7ihndon9swzedbAAAua/C.vzx6FIMw3OeBlitMcD.ufD7sC42AVwW', NULL, 'jordan.sipahutar@hotmail.com', 10, 1447735806, 1471257900);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggotakeluarga`
--
ALTER TABLE `anggotakeluarga`
  ADD PRIMARY KEY (`Kode_Peserta`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `charitywalk`
--
ALTER TABLE `charitywalk`
  ADD PRIMARY KEY (`Kode_Charitywalk`);

--
-- Indexes for table `citybike`
--
ALTER TABLE `citybike`
  ADD PRIMARY KEY (`Kode_Citybike`);

--
-- Indexes for table `futsal`
--
ALTER TABLE `futsal`
  ADD PRIMARY KEY (`Kode_Futsal`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `pesertacharitywalk`
--
ALTER TABLE `pesertacharitywalk`
  ADD PRIMARY KEY (`Kode_Peserta`);

--
-- Indexes for table `pesertafutsal`
--
ALTER TABLE `pesertafutsal`
  ADD PRIMARY KEY (`Kode_Peserta`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggotakeluarga`
--
ALTER TABLE `anggotakeluarga`
  MODIFY `Kode_Peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `charitywalk`
--
ALTER TABLE `charitywalk`
  MODIFY `Kode_Charitywalk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `citybike`
--
ALTER TABLE `citybike`
  MODIFY `Kode_Citybike` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `futsal`
--
ALTER TABLE `futsal`
  MODIFY `Kode_Futsal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pesertacharitywalk`
--
ALTER TABLE `pesertacharitywalk`
  MODIFY `Kode_Peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `pesertafutsal`
--
ALTER TABLE `pesertafutsal`
  MODIFY `Kode_Peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

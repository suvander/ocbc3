<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Citybike */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="citybike-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Nama_Peserta')->textInput(['maxlength' => true]) ?>
    
      <?= $form->field($model, 'Jenis_Kelamin')->dropDownList(Yii::$app->params['jeniskelamin']);?>
        
    <?= $form->field($model, 'Usia')->textInput(['maxlength' => true]) ?>

     <?= $form->field($model, 'NIK')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'Nama_Anggota_Keluarga')->textInput(['maxlength' => true]) ?>
    
    
    <?= $form->field($model, 'Email_Peserta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'No_HP')->textInput(['maxlength' => true]) ?>   

      <?=$form->field($model, 'Kategori_City_Bike')->dropDownList(Yii::$app->params['kategoricitybike']);?>

     <?=$form->field($model, 'Ukuran_Jersey')->dropDownList(Yii::$app->params['ukuranjersey']);?>

    
    
     <?=$form->field($model, 'Keterangan')->dropDownList(Yii::$app->params['jenissepeda']);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Register' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

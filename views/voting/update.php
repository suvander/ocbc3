<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Voting */

$this->title = 'Update Voting: ' . $model->Nama;
//$this->params['breadcrumbs'][] = ['label' => 'Votings', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->Kode_Voting, 'url' => ['view', 'id' => $model->Kode_Voting]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voting-update">

  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

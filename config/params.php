<?php

return [
    'adminEmail' => 'admin@example.com',
    'jeniskelamin' => array(
        'Pria' => 'Pria',
        'Wanita' => 'Wanita',
    ),
    'kategoricitybike' => array(
        '4,9 Km' => '4,9 Km',
        '49 Km' => '49 Km',
    ),
    'summary' => "Total :{totalCount}",
    'ukuranjersey' => array(
        'S Dewasa' => 'S Dewasa',
        'M Dewasa' => 'M Dewasa',
        'L Dewasa' => 'L Dewasa',
        'XL Dewasa' => 'XL Dewasa',
      // 'S Anak' => 'S Anak',
       // 'M Anak' => 'M Anak',
       // 'L Anak' => 'L Anak',
       // 'XL Anak' => 'XL Anak',
    ),
    'ukuranjerseycitybike' => array(
         'S Dewasa' => 'S Dewasa',
        'M Dewasa' => 'M Dewasa',
        'L Dewasa' => 'L Dewasa',
        'XL Dewasa' => 'XL Dewasa',
      //  'S Anak' => 'S Anak',
       // 'M Anak' => 'M Anak',
       // 'L Anak' => 'L Anak',
        //'XL Anak' => 'XL Anak',
    ),
    'jenissepeda' => array(
        'Road Bike' => 'Road Bike',
        'MTB' => 'MTB',
        'Sepeda Lipat' => 'Sepeda Lipat',
        'Lainnnya' => 'Lainnya'
    ),
    'senderMail'=>'event@49tahunindosatooredoo.com',
    'pendaftaransukses'=>'Pendaftaran anda sudah berhasil, email konfirmasi pendaftaran telah dikirimkan ke email anda. Jika email tidak diterima silahkan melakukan pengecekan di spam atau junk mail',
     'votingsukses'=>'Anda telah berhasil memilih foto favorit anda',
];

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Futsal;

/**
 * FutsalSearch represents the model behind the search form about `app\models\Futsal`.
 */
class FutsalSearch extends Futsal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Futsal'], 'integer'],
            [['Nama_Team', 'Nama_Manager_Team', 'Email_Perwakilan', 'No_HP', 'Keterangan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Futsal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Kode_Futsal' => $this->Kode_Futsal,
        ]);

        $query->andFilterWhere(['like', 'Nama_Team', $this->Nama_Team])
            ->andFilterWhere(['like', 'Nama_Manager_Team', $this->Nama_Manager_Team])
            ->andFilterWhere(['like', 'Email_Perwakilan', $this->Email_Perwakilan])
            ->andFilterWhere(['like', 'No_HP', $this->No_HP])
            ->andFilterWhere(['like', 'Keterangan', $this->Keterangan]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "anggotakeluarga".
 *
 * @property integer $Kode_Peserta
 * @property integer $Kode_Citybike
 * @property string $Nama
 * @property string $Jenis_Kelamin
 * @property string $Ukuran_Jersey
 * @property string $Usia
 * @property string $Jenis_Sepeda
 * @property string $field1
 * @property string $filed2
 * @property string $field3
 */
class Anggotakeluarga extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'anggotakeluarga';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Kode_Citybike', 'Nama', 'Jenis_Kelamin', 'Ukuran_Jersey', 'Usia', 'Jenis_Sepeda'], 'required'],
            [['Kode_Citybike'], 'integer'],
            [['Nama', 'Ukuran_Jersey', 'Usia', 'Jenis_Sepeda', 'field1', 'filed2', 'field3'], 'string', 'max' => 200],
            [['Jenis_Kelamin'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Kode_Peserta' => 'Kode  Peserta',
            'Kode_Citybike' => 'Peserta',
            'Nama' => 'Nama',
            'Jenis_Kelamin' => 'Jenis  Kelamin',
            'Ukuran_Jersey' => 'Ukuran  Jersey',
            'Usia' => 'Usia',
            'Jenis_Sepeda' => 'Jenis  Sepeda',
            'field1' => 'Jarak Tempuh',
            'filed2' => 'Filed2',
            'field3' => 'Field3',
        ];
    }
    
    public function getCitybike()
    {
        return $this->hasOne(Citybike::className(), ['Kode_Citybike' => 'Kode_Citybike']);
    }
}
